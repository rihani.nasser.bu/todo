import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:to_do/configration/config.dart';
import 'package:to_do/screens/screens.dart';

class NoteModel {
  String time;
  String note;
  String id;

  NoteModel({required this.time, required this.note, required this.id});

  static addNote({required addNoteController}) {
    FirebaseFirestore.instance
        .collection("users")
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .collection("notes")
        .add({
      "time": DateTime.now().millisecondsSinceEpoch,
      "note": addNoteController.text
    });
  }

  static editNote({required String noteText, required context}) {
    FirebaseFirestore.instance
        .collection("users")
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .collection("notes")
        .doc(Config.note.id)
        .update({
      "time": DateTime.now().millisecondsSinceEpoch,
      "note": noteText,
    });
    Navigator.pushNamedAndRemoveUntil(context, HomeScreen.id, (route) => false);
  }

  static deleteNote({required context}) {
    FirebaseFirestore.instance
        .collection("users")
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .collection("notes")
        .doc(Config.note.id)
        .delete();
    Navigator.pushNamedAndRemoveUntil(context, HomeScreen.id, (route) => false);
  }
}
