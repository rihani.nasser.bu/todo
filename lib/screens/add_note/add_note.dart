import 'package:flutter/material.dart';
import 'package:to_do/models/note_model.dart';
import 'package:to_do/screens/screens.dart';
import 'package:to_do/widgets/widget.dart';

class AddNote extends StatefulWidget {
  const AddNote({Key? key}) : super(key: key);
  static String id = "add";

  @override
  State<AddNote> createState() => _AddNoteState();
}

class _AddNoteState extends State<AddNote> {
  @override
  Widget build(BuildContext context) {
    TextEditingController addNoteController = TextEditingController();
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        title: const Text("Add A note"),
      ),
      body: Column(
        children: [
          TextFieldCustom(
              keyboardType: TextInputType.text,
              hide: false,
              controller: addNoteController,
              hint: "add note",
              label: "add note"),
          ElevatedButtonCustom(
              buttonText: "Add Note",
              onPressed: () {
                NoteModel.addNote(addNoteController: addNoteController);
                Navigator.pushNamedAndRemoveUntil(context, HomeScreen.id, (route) => false);
              }),
        ],
      ),
    ));
  }
}
