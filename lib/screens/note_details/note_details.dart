import 'package:flutter/material.dart';
import 'package:to_do/configration/config.dart';
import 'package:to_do/models/note_model.dart';
import 'package:to_do/widgets/widget.dart';

class NoteDetail extends StatefulWidget {
  const NoteDetail({Key? key}) : super(key: key);
  static String id = "note details";

  @override
  State<NoteDetail> createState() => _NoteDetailState();
}

class _NoteDetailState extends State<NoteDetail> {
  @override
  Widget build(BuildContext context) {
    TextEditingController editNote =
        TextEditingController(text: Config.note.note);
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        title: const Text("NoteDetail"),
      ),
      body: Column(
        children: [
          TextFieldCustom(
              keyboardType: TextInputType.text,
              hide: false,
              controller: editNote,
              hint: "",
              label: "",
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButtonCustom(
                onPressed: () {
                  NoteModel.editNote(noteText: editNote.text,context: context);
                },
                buttonText: "edit",
              ),
              ElevatedButtonCustom(
                onPressed: () {
                  NoteModel.deleteNote(context: context);
                },
                buttonText: "delete",
              ),
            ],
          )
        ],
      ),
    ));
  }
}
