import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:to_do/screens/screens.dart';

class Splash extends StatefulWidget {
  const Splash({Key? key}) : super(key: key);
  static String id = "splash";

  @override
  State<Splash> createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  checkUser() async {
    var sp = await SharedPreferences.getInstance();
    var userid = sp.getString("id")??"";
    if (userid != "") {
      Navigator.pushNamed(context, HomeScreen.id);

    } else {
      Navigator.pushNamed(context, LoginScreen.id);
    }
  }

  @override
  void initState() {
    checkUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return const SafeArea(
        child: Scaffold(
      body: CircularProgressIndicator(),
    ));
  }
}
