import 'package:flutter/material.dart';
import 'package:to_do/models/user_model.dart';
import 'package:to_do/screens/screens.dart';
import 'package:to_do/widgets/widget.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);
  static String id = "login";
  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    TextEditingController emailController = TextEditingController();
    TextEditingController passwordController = TextEditingController();
    String emailText = "";
    String passwordText = "";
    return SafeArea(
        child: Scaffold(
      body: Column(
        children: [
          TextFieldCustom(
            keyboardType: TextInputType.emailAddress,
            hide: false,
            controller: emailController,
            hint: "Email",
            label: "Email",
            onChange: (value) {
              emailText = value;
            },
          ),
          TextFieldCustom(
            keyboardType: TextInputType.text,
            hide: true,
            controller: passwordController,
            hint: "password",
            label: "password",
            onChange: (value) {
              passwordText = value;
            },
          ),
          ElevatedButtonCustom(
            onPressed: (){
              User.login(context: context, email: emailText, password: passwordText);
            },
            buttonText: "Login",
          ),
          TextButton(onPressed: (){
            Navigator.restorablePushNamedAndRemoveUntil(context, SignUpScreen.id, (route) => false);
            }, child: const Text("Don't have an account?\nsginup",textAlign: TextAlign.center,))
        ],
      ),
    ));
  }
}
