import 'package:flutter/material.dart';
import 'package:to_do/screens/screens.dart';
import 'package:to_do/widgets/widget.dart';
import 'package:to_do/models/user_model.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);
  static String id = "signup";

  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  @override
  Widget build(BuildContext context) {
    TextEditingController emailController = TextEditingController();
    TextEditingController passwordController = TextEditingController();
    String emailText = "";
    String passwordText = "";
    return SafeArea(
        child: Scaffold(
      body: Column(
        children: [
          TextFieldCustom(
            keyboardType: TextInputType.emailAddress,
            hide: false,
            controller: emailController,
            hint: "Email",
            label: "Email",
            onChange: (value) {
              emailText = value;
            },
          ),
          TextFieldCustom(
            keyboardType: TextInputType.text,
            hide: true,
            controller: passwordController,
            hint: "password",
            label: "password",
            onChange: (value) {
              passwordText = value;
            },
          ),
          ElevatedButtonCustom(
            onPressed: () {
              User.signup(email:  emailText,password:  passwordText,context: context);
            },
            buttonText: "Signup",
          ),
          TextButton(
              onPressed: () {
                Navigator.restorablePushNamedAndRemoveUntil(
                    context, LoginScreen.id, (route) => false);
              },
              child: const Text(
                "Do you have an account?\nlogin",
                textAlign: TextAlign.center,
              ))
        ],
      ),
    ));
  }
}
