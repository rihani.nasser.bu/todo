import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:to_do/screens/screens.dart';
import 'package:to_do/widgets/note_card.dart';
class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);
  static String id = "home";
  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(onPressed: (){FirebaseAuth.instance.signOut();Navigator.pushNamedAndRemoveUntil(context, LoginScreen.id, (route) => false);}, icon: const Icon(Icons.logout)),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            StreamBuilder<QuerySnapshot>(
              stream: FirebaseFirestore.instance.collection("users").doc(FirebaseAuth.instance.currentUser!.uid).collection("notes").orderBy("time",descending: true).snapshots(),
              builder: (context, snapshot) {
                List<NoteCard> notes =[];
                if(snapshot.hasData){
                  for(var data in snapshot.data!.docs){
                    notes.add(NoteCard(note: data["note"], id: data.id, time: data["time"].toString()));
                  }
                  return Column(
                    children: notes,
                  );
                }else{
                return  const Text("no data");
                }
              },

            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          Navigator.pushNamed(context, AddNote.id);
        },
        child: const Icon(Icons.add),
      ),
    ),
    );
  }
}
