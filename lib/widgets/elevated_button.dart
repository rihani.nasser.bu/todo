import 'package:flutter/material.dart';

class ElevatedButtonCustom extends StatelessWidget {
  String buttonText;
  var onPressed;

  ElevatedButtonCustom({Key? key, required this.buttonText,required this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: ElevatedButton(
          onPressed: onPressed,
          child: Text(buttonText),
        ));
  }
}
