import 'package:flutter/material.dart';
import 'package:to_do/configration/config.dart';
import 'package:to_do/screens/note_details/note_details.dart';
class NoteCard extends StatelessWidget {
  String note;
  String id;
  String time;


  NoteCard({Key? key, required this.note, required this.id, required this.time}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        Config.note.note = note;
        Config.note.time = time;
        Config.note.id = id;
        Navigator.pushNamed(context, NoteDetail.id);
      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Text(note)
          ],
        ),
      ),
    );
  }
}
