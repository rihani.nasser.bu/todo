import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:to_do/screens/screens.dart';

void main()async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        Splash.id : (context) => const Splash(),
        LoginScreen.id : (context) => const LoginScreen(),
        SignUpScreen.id : (context) => const SignUpScreen(),
        HomeScreen.id : (context) => const HomeScreen(),
        AddNote.id : (context) => const AddNote(),
        NoteDetail.id : (context) => const NoteDetail(),

      },
      initialRoute: Splash.id,
    );
  }
}